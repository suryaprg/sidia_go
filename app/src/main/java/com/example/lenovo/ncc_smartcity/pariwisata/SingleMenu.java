package com.example.lenovo.ncc_smartcity.pariwisata;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.lenovo.ncc_smartcity.R;
import com.example.lenovo.ncc_smartcity._globalVariable.StaticVariable;
import com.example.lenovo.ncc_smartcity._globalVariable.URLCollection;
import com.example.lenovo.ncc_smartcity.maps.DirectionFinder;
import com.example.lenovo.ncc_smartcity.maps.DirectionFinderListener;
import com.example.lenovo.ncc_smartcity.maps.Route;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class SingleMenu extends FragmentActivity implements OnMapReadyCallback, DirectionFinderListener {

    //location now
    LocationManager locationManager;
    String provider, loc_prov;
    Double lat, lng;
    String jalan;
    Handler handler;

    //maps
    String TAG = "SingleMenu";
    private GoogleMap mMap;
    private List<Marker> originMarkers = new ArrayList<>();
    private List<Marker> destinationMarkers = new ArrayList<>();
    private List<Polyline> polylinePaths = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        try{
        setContentView(R.layout.pariwisata_single_menu);
//            Log.e(TAG, "onCreate: start");
//        }catch (Exception e){
//            Log.e(TAG, "onCreate: "+e);
//        }

//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);

        ImageView google = findViewById(R.id.location);
        final TextView nama = findViewById(R.id.nama);
        final TextView deskripsi = findViewById(R.id.deskripsi);
        final ImageView gambar = findViewById(R.id.gambar);
//        ImageView gambar1 = findViewById(R.id.gambar1);
        TypedArray icons = getResources().obtainTypedArray(R.array.list_icon);
        final TextView keterangan = findViewById(R.id.keterangan);
        final TextView telepon = findViewById(R.id.telepon);
        TextView textView2 = findViewById(R.id.tv_pariwisata_list);

        Intent intent = getIntent();
        final int a = intent.getIntExtra("id", -1);
        final String b = intent.getStringExtra("selected");
        final String titlesEx = intent.getStringExtra("titles");
        final String alamatEx = intent.getStringExtra("alamat");
        final String deskripsiEx = intent.getStringExtra("deskripsi");
        final String teleponEx = intent.getStringExtra("telepon");
        final String gambarEx = intent.getStringExtra("gambar");
        nama.setText(titlesEx);
        deskripsi.setText(alamatEx);
        Picasso.with(SingleMenu.this).load(gambarEx).into(gambar);
        jalan = alamatEx;
        keterangan.setText(deskripsiEx);
        telepon.setText(teleponEx);


        if (Build.VERSION.SDK_INT < 19) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            Log.e(TAG, "onCreate: " + policy);
        }

        if (ActivityCompat.checkSelfPermission(SingleMenu.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(SingleMenu.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            Log.e("TAG", "onMapReady: fail");
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions();
//            }
        }

        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("google.navigation:q=" + jalan));
                    //Uri.parse("http://maps.google.com/maps?saddr="+lat.toString()+","+lng.toString()+"&daddr="+jalan));
                    Log.e(TAG, "onClick: " + intent);
                    startActivity(intent);

                } catch (Exception e) {
                    Log.e(TAG, "onClick: " + e);
                }
            }
        });

//bismillah
    }


    private void requestPermissions() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 2);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;

//        LatLng sydney = new LatLng(lat, lng);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("sweet"));
//        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 14));

        }
    }

    private void sendRequest() {
        //set location string start and finish
        String st = lat.toString() + "," + lng.toString();
        String fn = jalan;

        if (st.isEmpty() && fn.isEmpty()) {
            Toast.makeText(SingleMenu.this, "isi point start dan finish dengan tepat", Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            new DirectionFinder(this, st, fn).execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            Toast.makeText(SingleMenu.this, e.toString(), Toast.LENGTH_SHORT).show();

        }

    }

    @Override
    public void onDirectionFinderStart() {
        //cek markers on map if markers are avaible do clear all markers
        try {
            Log.e("marker", "delet marker");
            if (originMarkers != null) {
                for (Marker marker : originMarkers) {
                    marker.remove();

                }
            }

            if (destinationMarkers != null) {
                for (Marker marker : destinationMarkers) {
                    marker.remove();
                }
            }

            if (polylinePaths != null) {
                for (Polyline polyline : polylinePaths) {
                    polyline.remove();
                }
            }
        } catch (Exception e) {

            //Log.e("onDirectionFinderStart", "onDirectionFinderStart: "+e.toString());

        }

    }


    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {
        //if direction success call marker origin and destination then call lat,lng route
        polylinePaths = new ArrayList<>();
        originMarkers = new ArrayList<>();
        destinationMarkers = new ArrayList<>();

        for (Route route : routes) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 15));

            //display markers
            originMarkers.add(mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.start_blue))
                    .title(route.startAddress)
                    .position(route.startLocation)));
            destinationMarkers.add(mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.end_green))
                    .title(route.endAddress)
                    .position(route.endLocation)));

            //set polyline layout
            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true).color(Color.BLUE).clickable(true).
                    width(5);

            //draw list route on map
            for (int i = 0; i < route.points.size(); i++)
                polylineOptions.add(route.points.get(i));

            //i dont know coy
            polylinePaths.add(mMap.addPolyline(polylineOptions));
        }
    }

    private void request_permission() {
        ActivityCompat.requestPermissions(SingleMenu.this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        ActivityCompat.requestPermissions(SingleMenu.this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 2);
    }

}


