package com.example.lenovo.ncc_smartcity.lowongan_pekerjaan.model_loker;

import com.google.gson.annotations.SerializedName;

/**
 * Created by USER on 21/03/2018.
 */

public class List_Item {
    @SerializedName("id_lowongan")
    String id_lowongan;
    @SerializedName("nama_perusahaan")
    String nama_perusahaan;
    @SerializedName("batas_waktu")
    String batas_waktu;
    @SerializedName("syarat")
    String syarat;
    @SerializedName("kualifikasi_skill")
    String kualifikasi_skill;
    @SerializedName("deskripsi")
    String deskripsi;
    @SerializedName("gaji")
    String gaji;
    @SerializedName("nama_category")
    String nama_category;
    @SerializedName("nama_bidang")
    String nama_bidang;
    @SerializedName("nama_lv")
    String nama_lv;
    @SerializedName("nama_prodi")
    String nama_prodi;
    @SerializedName("nama_status")
    String nama_status;

    public List_Item(String id_lowongan, String nama_perusahaan, String batas_waktu, String syarat, String kualifikasi_skill, String deskripsi, String gaji, String nama_category, String nama_bidang, String nama_lv, String nama_prodi, String nama_status) {
        this.id_lowongan = id_lowongan;
        this.nama_perusahaan = nama_perusahaan;
        this.batas_waktu = batas_waktu;
        this.syarat = syarat;
        this.kualifikasi_skill = kualifikasi_skill;
        this.deskripsi = deskripsi;
        this.gaji = gaji;
        this.nama_category = nama_category;
        this.nama_bidang = nama_bidang;
        this.nama_lv = nama_lv;
        this.nama_prodi = nama_prodi;
        this.nama_status = nama_status;
    }

    public String getId_lowongan() {
        return id_lowongan;
    }

    public void setId_lowongan(String id_lowongan) {
        this.id_lowongan = id_lowongan;
    }

    public String getNama_perusahaan() {
        return nama_perusahaan;
    }

    public void setNama_perusahaan(String nama_perusahaan) {
        this.nama_perusahaan = nama_perusahaan;
    }

    public String getBatas_waktu() {
        return batas_waktu;
    }

    public void setBatas_waktu(String batas_waktu) {
        this.batas_waktu = batas_waktu;
    }

    public String getSyarat() {
        return syarat;
    }

    public void setSyarat(String syarat) {
        this.syarat = syarat;
    }

    public String getKualifikasi_skill() {
        return kualifikasi_skill;
    }

    public void setKualifikasi_skill(String kualifikasi_skill) {
        this.kualifikasi_skill = kualifikasi_skill;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getGaji() {
        return gaji;
    }

    public void setGaji(String gaji) {
        this.gaji = gaji;
    }

    public String getNama_category() {
        return nama_category;
    }

    public void setNama_category(String nama_category) {
        this.nama_category = nama_category;
    }

    public String getNama_bidang() {
        return nama_bidang;
    }

    public void setNama_bidang(String nama_bidang) {
        this.nama_bidang = nama_bidang;
    }

    public String getNama_lv() {
        return nama_lv;
    }

    public void setNama_lv(String nama_lv) {
        this.nama_lv = nama_lv;
    }

    public String getNama_prodi() {
        return nama_prodi;
    }

    public void setNama_prodi(String nama_prodi) {
        this.nama_prodi = nama_prodi;
    }

    public String getNama_status() {
        return nama_status;
    }

    public void setNama_status(String nama_status) {
        this.nama_status = nama_status;
    }
}
