package com.example.lenovo.ncc_smartcity.lowongan_pekerjaan.model_loker;


import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by USER on 13/03/2018.
 */

public class ResponseGet {
    @SerializedName("id_status")
    String id_status;

    @SerializedName("status")
    String status;

    @SerializedName("message")
    String mesage;

    @SerializedName("list_item")
    List<com.example.lenovo.ncc_smartcity.lowongan_pekerjaan.model_loker.List_Item> List_Item;

    public String getId_status() {
        return id_status;
    }

    public void setId_status(String id_status) {
        this.id_status = id_status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMesage() {
        return mesage;
    }

    public void setMesage(String mesage) {
        this.mesage = mesage;
    }

    public List<com.example.lenovo.ncc_smartcity.lowongan_pekerjaan.model_loker.List_Item> getList_Item() {
        return List_Item;
    }

    public void setList_Item(List<com.example.lenovo.ncc_smartcity.lowongan_pekerjaan.model_loker.List_Item> list_Item) {
        List_Item = list_Item;
    }
}
