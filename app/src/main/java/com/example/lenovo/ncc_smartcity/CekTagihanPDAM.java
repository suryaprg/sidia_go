package com.example.lenovo.ncc_smartcity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

public class CekTagihanPDAM extends Activity {

    Button btn_hapus, btn_cekTagihan;

    LinearLayout ll_hasilCekTagihan;

    EditText et_nomorTagihan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cek_tagihan_pdam);

        et_nomorTagihan = findViewById(R.id.et_nomorTagihanPDAM);

        ll_hasilCekTagihan = findViewById(R.id.ll_cekTagihanHasil);

        btn_cekTagihan = findViewById(R.id.btn_cekTagihan);
        btn_cekTagihan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_hasilCekTagihan.setVisibility(View.VISIBLE);
            }
        });

        btn_hapus = findViewById(R.id.btn_hapus);
        btn_hapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_nomorTagihan.setText("");
            }
        });
    }
}
