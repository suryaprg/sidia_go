package com.example.lenovo.ncc_smartcity.internalLib.RetrofitLib.maps_activity;

import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import java.util.List;
import java.util.Locale;

/**
 * Created by nemesis on 3/18/2018.
 */

public class InformationLocation extends Activity {

    String TAG = "InformationLocation";
    Context context;
    String getEventLocation;

    public InformationLocation(String getEventLocation) {
        this.getEventLocation = getEventLocation;
    }


//    public InformationLocation(String getEventLocation, Adapter_tanggap context) {
//        this.getEventLocation = getEventLocation;
//        this.context = context;
//    }

    public String getCompleteAddressString() {
        String strAdd = "";

        Log.e(TAG, "getCompleteAddressString: " + getEventLocation);
        String[] locData = getEventLocation.split(",");

        Log.e(TAG, "getCompleteAddressString: " + locData[1]);
        Geocoder geocoder = new Geocoder(InformationLocation.this, Locale.getDefault());

        Log.e(TAG, "getCompleteAddressString: " + geocoder);
        try {
            List<Address> addresses = geocoder.getFromLocation(Double.valueOf(locData[0]), Double.valueOf(locData[1]), 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();

                Log.e(TAG, "getCompleteAddressString: " + strReturnedAddress);
                //Toast.makeText(PostinganActivity.this, "My Current loction address = "+strReturnedAddress.toString(), Toast.LENGTH_LONG).show();
            } else {
//                Toast.makeText(PostinganActivity.this, "My Current loction address = No Address returned!", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "getCompleteAddressString: " + e);
//            Toast.makeText(PostinganActivity.this, "My Current loction address = Canont get Address!", Toast.LENGTH_LONG).show();
        }
        return strAdd;
    }
}
