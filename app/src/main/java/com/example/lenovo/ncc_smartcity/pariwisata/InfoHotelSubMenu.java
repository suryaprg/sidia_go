package com.example.lenovo.ncc_smartcity.pariwisata;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.lenovo.ncc_smartcity.R;
import com.example.lenovo.ncc_smartcity._globalVariable.StaticVariable;
import com.example.lenovo.ncc_smartcity._globalVariable.URLCollection;
import com.example.lenovo.ncc_smartcity.event.CustomAdapterEvent;
import com.example.lenovo.ncc_smartcity.event.DataModel;
import com.example.lenovo.ncc_smartcity.main_feature.Event;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by USER on 26/02/2018.
 */

public class InfoHotelSubMenu extends Activity {

    TextView textView;
    String selectedFeature;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pariwisata_layout);

        textView = findViewById(R.id.tv_pariwisata_list);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            selectedFeature = (String) bundle.get(StaticVariable.NAMA_LAYANAN);
            //Toast.makeText(this, selectedFeature, Toast.LENGTH_SHORT).show();
        }
        String url = null;
        switch (selectedFeature) {
            case StaticVariable.INFO_HOTEL:
                textView.setText("Info Hotel");
                //"http://192.168.86.25/sidia/c_wisata/getEaWisata/1"
                url = "http://sidia.malangkota.go.id/c_wisata/getEaWisata/1";
                new DapatWisata().DapatSemuaWisata(url, InfoHotelSubMenu.this, selectedFeature);
                break;

            case StaticVariable.INFO_HOMESTAY:
                textView.setText("Info Homestay");
                url = "http://sidia.malangkota.go.id/c_wisata/getEaWisata/4" ;
                new DapatWisata().DapatSemuaWisata(url, InfoHotelSubMenu.this, selectedFeature);
                break;

            case StaticVariable.INFO_GUESTHOUSE:
                textView.setText("Info Guesthouse");
                url = "http://sidia.malangkota.go.id/c_wisata/getEaWisata/3";
                new DapatWisata().DapatSemuaWisata(url, InfoHotelSubMenu.this, selectedFeature);
                break;

        }
    }

}