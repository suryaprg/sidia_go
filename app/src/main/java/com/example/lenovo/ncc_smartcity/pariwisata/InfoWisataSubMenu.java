package com.example.lenovo.ncc_smartcity.pariwisata;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.lenovo.ncc_smartcity.R;
import com.example.lenovo.ncc_smartcity._globalVariable.StaticVariable;
import com.example.lenovo.ncc_smartcity._globalVariable.URLCollection;
import com.example.lenovo.ncc_smartcity.event.CustomAdapterEvent;
import com.example.lenovo.ncc_smartcity.event.DataModel;
import com.example.lenovo.ncc_smartcity.main_feature.Event;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by lenovo on 2/5/2018.
 */

public class InfoWisataSubMenu extends Activity {

    TextView textView;
    String selectedFeature;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pariwisata_layout);

        textView = findViewById(R.id.tv_pariwisata_list);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            selectedFeature = (String) bundle.get(StaticVariable.NAMA_LAYANAN);
            //Toast.makeText(this, selectedFeature, Toast.LENGTH_SHORT).show();
        }
        String url = null;
        switch (selectedFeature) {
            case StaticVariable.INFO_SEJARAH:
                textView.setText("Info Sejarah");
                url = "http://sidia.malangkota.go.id/c_wisata/getEaWisata/12";
                new DapatWisata().DapatSemuaWisata(url, InfoWisataSubMenu.this, selectedFeature);
                break;


            case StaticVariable.INFO_EDUKASI:
                textView.setText("Info Edukasi");
                url = "http://sidia.malangkota.go.id/c_wisata/getEaWisata/13";
                new DapatWisata().DapatSemuaWisata(url, InfoWisataSubMenu.this, selectedFeature);
                break;


            case StaticVariable.INFO_REKREASI:
                textView.setText("Info Rekreasi");
                url = "http://sidia.malangkota.go.id/c_wisata/getEaWisata/7";
                new DapatWisata().DapatSemuaWisata(url, InfoWisataSubMenu.this, selectedFeature);

                break;

            case StaticVariable.INFO_TAMAN_HUTAN:
                textView.setText("Info Taman dan Hutan Kota");
                url = "http://sidia.malangkota.go.id/c_wisata/getEaWisata/8" ;
                new DapatWisata().DapatSemuaWisata(url, InfoWisataSubMenu.this, selectedFeature);
                break;

            case StaticVariable.INFO_BELANJA:
                textView.setText("Info Wisata Belanja");
                url = "http://sidia.malangkota.go.id/c_wisata/getEaWisata/10";
                new DapatWisata().DapatSemuaWisata(url, InfoWisataSubMenu.this, selectedFeature);
                break;

            case StaticVariable.INFO_RELIGI:
                textView.setText("Info Religi");

                url = "http://sidia.malangkota.go.id/c_wisata/getEaWisata/14" ;
                new DapatWisata().DapatSemuaWisata(url, InfoWisataSubMenu.this, selectedFeature);
                break;


            case StaticVariable.INFO_OLAHRAGA:
                textView.setText("Info Olahraga");
                url = "http://sidia.malangkota.go.id/c_wisata/getEaWisata/9";
                new DapatWisata().DapatSemuaWisata(url, InfoWisataSubMenu.this, selectedFeature);
                break;

            case StaticVariable.INFO_KAMPUNG:
                textView.setText("Info Kampung Wisata");
                url = "http://sidia.malangkota.go.id/c_wisata/getEaWisata/5";
                new DapatWisata().DapatSemuaWisata(url, InfoWisataSubMenu.this, selectedFeature);
                break;


            case StaticVariable.INFO_KULINER:
                textView.setText("Info Kuliner");
                url = "http://sidia.malangkota.go.id/c_wisata/getEaWisata/2";
                new DapatWisata().DapatSemuaWisata(url, InfoWisataSubMenu.this, selectedFeature);
                break;

            case StaticVariable.INFO_OLEH_OLEH:
                textView.setText("Info Oleh Oleh");
                url = "http://sidia.malangkota.go.id/c_wisata/getEaWisata/11" ;
                new DapatWisata().DapatSemuaWisata(url, InfoWisataSubMenu.this, selectedFeature);
                break;


        }
    }

}