package com.example.lenovo.ncc_smartcity.event;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.lenovo.ncc_smartcity.R;
import com.example.lenovo.ncc_smartcity.main_feature.Event;
import com.example.lenovo.ncc_smartcity.main_feature.KumpulanLayanan;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;


/**
 * Created by lenovo on 2/5/2018.
 */

public class EventAdd extends AppCompatActivity {

    //    ListView listView;
    private static CustomAdapterEvent adapter;
    ArrayList<DataModel> dataModels;
    ProgressDialog progressDialog;
    String loadingText = "Loading";
    Button submit, gambarGaleri;

    ImageView gambar;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_add);
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white_24px));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        gambarGaleri = (Button) findViewById(R.id.galeri);
        submit = (Button) findViewById(R.id.submit_event);
        gambar = (ImageView) findViewById(R.id.preview_gambar);
//        final EditText nama=(EditText) findViewById(R.id.nama);
//        final EditText deskripsi=(EditText) findViewById(R.id.description);
//        final EditText kategori=(EditText) findViewById(R.id.kategori);

        gambarGaleri.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);//
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                final Dialog dialog = new Dialog(v.getContext());
                dialog.setContentView(R.layout.event_dialog_lihat);
                dialog.setTitle("Poster");

new UploadImageTask(gambar,v).execute();

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case 0:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    gambar.setImageURI(selectedImage);
                }

                break;
            case 1:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    gambar.setImageURI(selectedImage);
                }
                break;
        }
    }

    //hai
    @Override
    protected void onDestroy() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        super.onDestroy();
    }


}
