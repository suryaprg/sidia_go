package com.example.lenovo.ncc_smartcity.lowongan_pekerjaan;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lenovo.ncc_smartcity.R;
import com.example.lenovo.ncc_smartcity._globalVariable.StaticVariable;
import com.example.lenovo.ncc_smartcity.lowongan_pekerjaan.model_loker.List_Item;

import java.util.List;

/**
 * Created by arimahardika on 19/03/2018.
 */

public class Adapter_Loker extends RecyclerView.Adapter<Adapter_Loker.LokerViewHolder> {

    String TAG = "Adapter_Loker";
    private List<List_Item> listLoker;
    private Context context;

    public Adapter_Loker(List<List_Item> listLoker, Context context) {
        this.listLoker = listLoker;
        this.context = context;
    }

    @Override
    public LokerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.loker_list_row, parent, false);
        return new LokerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LokerViewHolder holder, int position) {
        List_Item model_loker = listLoker.get(position);

        final String id_lowongan = model_loker.getId_lowongan();
        final String nama_perusahaan = model_loker.getNama_perusahaan();
        final String batas_waktu = "Berlaku Sampai : " + model_loker.getBatas_waktu();
        final String syarat = model_loker.getSyarat();
        final String kualifikasi_skill = model_loker.getKualifikasi_skill();
        final String deskripsi = model_loker.getDeskripsi();
        final String gaji = model_loker.getGaji();
        String nama_category = model_loker.getNama_category();
        String nama_bidang = model_loker.getNama_bidang();
        String nama_lv = model_loker.getNama_lv();
        String nama_prodi = model_loker.getNama_prodi();
        String nama_status = model_loker.getNama_status();

        final String nama_posisi = nama_lv + " " + nama_bidang;
        final String kategori_prodi = nama_category + " - " + nama_prodi;

        Log.e(TAG, "onBindViewHolder: " + id_lowongan +
                "---" + nama_perusahaan +
                "---" + batas_waktu +
                "---" + syarat +
                "---" + kualifikasi_skill +
                "---" + deskripsi +
                "---" + gaji +
                "---" + nama_category +
                "---" + nama_bidang +
                "---" + nama_lv +
                "---" + nama_prodi +
                "---" + nama_status
        );

        holder.tv_idLoker.setText(id_lowongan);
        holder.tv_posisi.setText(nama_posisi);
        holder.tv_namaPerusahaan.setText(nama_perusahaan);
        holder.tv_lokasi.setText(kategori_prodi);
        holder.tv_batasWaktu.setText(batas_waktu);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Description_Loker.class);

                intent.putExtra(StaticVariable.LOKER_ID_LOWONGAN, id_lowongan);
                intent.putExtra(StaticVariable.LOKER_NAMA_POSISI, nama_posisi);
                intent.putExtra(StaticVariable.LOKER_NAMA_PERUSAHAAN, nama_perusahaan);
                intent.putExtra(StaticVariable.LOKER_KATEGORI_PRODI, kategori_prodi);
                intent.putExtra(StaticVariable.LOKER_KUALIFIKASI_SKILL, kualifikasi_skill);
                intent.putExtra(StaticVariable.LOKER_PERSYARATAN, syarat);
                intent.putExtra(StaticVariable.LOKER_DESKRIPSI, deskripsi);
                intent.putExtra(StaticVariable.LOKER_GAJI, gaji);
                intent.putExtra(StaticVariable.LOKER_BATAS_WAKTU, batas_waktu);

                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listLoker.size();
    }

    public class LokerViewHolder extends RecyclerView.ViewHolder {

        TextView tv_idLoker, tv_posisi, tv_namaPerusahaan, tv_lokasi, tv_batasWaktu;

        CardView cardView;

        public LokerViewHolder(View itemView) {
            super(itemView);

            tv_idLoker = itemView.findViewById(R.id.tv_loker_id);
            tv_posisi = itemView.findViewById(R.id.tv_loker_posisi);
            tv_namaPerusahaan = itemView.findViewById(R.id.tv_loker_namaPerusahaan);
            tv_lokasi = itemView.findViewById(R.id.tv_loker_kategori_bidang);
            tv_batasWaktu = itemView.findViewById(R.id.tv_loker_deadline);
            cardView = itemView.findViewById(R.id.cardview_loker);
        }
    }
}
