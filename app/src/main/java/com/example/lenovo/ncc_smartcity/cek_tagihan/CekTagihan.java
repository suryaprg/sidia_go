package com.example.lenovo.ncc_smartcity.cek_tagihan;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.lenovo.ncc_smartcity.CekTagihanPDAM;
import com.example.lenovo.ncc_smartcity.R;
import com.makeramen.roundedimageview.RoundedImageView;

public class CekTagihan extends Activity {

    RoundedImageView riv_pdam, riv_pln, riv_telkom;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cek_tagihan_home);

        riv_pdam = findViewById(R.id.riv_cek_tagihan_pdam);
        riv_pdam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(CekTagihan.this, CekTagihanPDAM.class);
                startActivity(intent);
            }
        });
    }
}
