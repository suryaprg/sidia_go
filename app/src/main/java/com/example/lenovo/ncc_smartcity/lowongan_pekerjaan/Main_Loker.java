package com.example.lenovo.ncc_smartcity.lowongan_pekerjaan;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.lenovo.ncc_smartcity.R;
import com.example.lenovo.ncc_smartcity._globalVariable.URLCollection;
import com.example.lenovo.ncc_smartcity.internalLib.RetrofitLib.base_url.Base_url;
import com.example.lenovo.ncc_smartcity.internalLib.RetrofitLib.request_management.SendDataLoker;
import com.example.lenovo.ncc_smartcity.lowongan_pekerjaan.model_loker.List_Item;
import com.example.lenovo.ncc_smartcity.lowongan_pekerjaan.model_loker.ResponseGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by arimahardika on 19/03/2018.
 */

public class Main_Loker extends Activity {

    String DATA_SOURCE_URL = URLCollection.DATA_SOURCE_LOKER;
    String TOKEN_MOBILE = "X00W";
    RecyclerView recyclerView;
    String TAG = "Main_Loker";
    List<List_Item> list_loker = new ArrayList<>();
    Call<ResponseGet> getdata;
    SendDataLoker base_url_management;
    ProgressDialog progressDialog;
    String loadingMessage = "Loading";
    LinearLayout ll_sumber_loker;
    private RecyclerView.Adapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_loker);

        ll_sumber_loker = findViewById(R.id.ll_sumber_loker);
        recyclerView = findViewById(R.id.loker_recyclerView);
        recyclerView.hasFixedSize();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        getDataLoker();
    }

    @Override
    protected void onDestroy() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        super.onDestroy();
    }

    private void getDataLoker() {
        progressDialog = new ProgressDialog(this) {
            @Override
            public void onBackPressed() {
                finish();
            }
        };

        progressDialog.setMessage(loadingMessage);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();

        RequestBody token = MultipartBody.create(MediaType.parse("multipart/form-data"), TOKEN_MOBILE);

        base_url_management = Base_url.getClient(DATA_SOURCE_URL).create(SendDataLoker.class);

        getdata = base_url_management.getItemAll(token);

        adapterRequest(getdata);

    }

    public void adapterRequest(Call<ResponseGet> getdata) {
        Log.e(TAG, "adapterRequest: run");
        getdata.enqueue(new Callback<ResponseGet>() {
            @Override
            public void onResponse(Call<ResponseGet> call, retrofit2.Response<ResponseGet> response) {
                progressDialog.hide();
                try {
                    list_loker = response.body().getList_Item();
                    //home = 0; dashbrd = 1; search = 2;
                    Log.e(TAG, "onResponse: " + response.body().getList_Item().get(0).getDeskripsi());

                    mAdapter = new Adapter_Loker(list_loker, Main_Loker.this);
                    recyclerView.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();

                } catch (Exception e) {
                    Log.e(TAG, "onResponse: " + e);
                }
            }

            @Override
            public void onFailure(Call<ResponseGet> call, Throwable t) {
                progressDialog.hide();
                Toast.makeText(Main_Loker.this, "Request Failure Please Refresh your Apps :)", Toast.LENGTH_LONG).show();
                Log.e(TAG, "onFailure: " + t);
            }
        });
    }
}
