package com.example.lenovo.ncc_smartcity.lowongan_pekerjaan;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.example.lenovo.ncc_smartcity.R;
import com.example.lenovo.ncc_smartcity._globalVariable.StaticVariable;

/**
 * Created by arimahardika on 28/03/2018.
 */

public class Description_Loker extends Activity {

    String str_idLoker, str_posisi, str_namaPerusahaan, str_kategoriBidang,
            str_persyaratan, str_kemampuanMinimal, str_deskripsi, str_gaji,
            str_deadline;

    TextView tv_idLoker, tv_posisi, tv_namaPerusahaan, tv_kategoriBidang,
            tv_persyaratan, tv_kemampuanMinimal, tv_deskripsi, tv_gaji,
            tv_deadline;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loker_description);

        getExtra();

        tv_idLoker = findViewById(R.id.tv_lokerDesc_id);
        tv_posisi = findViewById(R.id.tv_lokerDesc_posisi);
        tv_namaPerusahaan = findViewById(R.id.tv_lokerDesc_namaPerusahaan);
        tv_kategoriBidang = findViewById(R.id.tv_lokerDesc_kategori_bidang);
        tv_persyaratan = findViewById(R.id.tv_lokerDesc_persyaratan);
        tv_kemampuanMinimal = findViewById(R.id.tv_lokerDesc_skill);
        tv_deskripsi = findViewById(R.id.tv_lokerDesc_deskripsi);
        tv_gaji = findViewById(R.id.tv_lokerDesc_gaji);
        tv_deadline = findViewById(R.id.tv_lokerDesc_deadline);

        populateView();
    }

    private void populateView() {
        tv_idLoker.setText(str_idLoker);
        tv_posisi.setText(str_posisi);
        tv_namaPerusahaan.setText(str_namaPerusahaan);
        tv_kategoriBidang.setText(str_kategoriBidang);
        tv_persyaratan.setText(str_persyaratan);
        tv_kemampuanMinimal.setText(str_kemampuanMinimal);
        tv_deskripsi.setText(str_deskripsi);
        tv_gaji.setText(str_gaji);
        tv_deadline.setText(str_deadline);
    }

    private void getExtra() {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            str_idLoker = (String) bundle.get(StaticVariable.LOKER_ID_LOWONGAN);
            str_posisi = (String) bundle.get(StaticVariable.LOKER_NAMA_POSISI);
            str_namaPerusahaan = (String) bundle.get(StaticVariable.LOKER_NAMA_PERUSAHAAN);
            str_kategoriBidang = (String) bundle.get(StaticVariable.LOKER_KATEGORI_PRODI);
            str_persyaratan = (String) bundle.get(StaticVariable.LOKER_PERSYARATAN);
            str_kemampuanMinimal = (String) bundle.get(StaticVariable.LOKER_KUALIFIKASI_SKILL);
            str_deskripsi = (String) bundle.get(StaticVariable.LOKER_DESKRIPSI);
            str_gaji = (String) bundle.get(StaticVariable.LOKER_GAJI);
            str_deadline = (String) bundle.get(StaticVariable.LOKER_BATAS_WAKTU);
        }
    }
}
