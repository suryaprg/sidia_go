package com.example.lenovo.ncc_smartcity.internalLib.RetrofitLib.request_management;


import com.example.lenovo.ncc_smartcity.lowongan_pekerjaan.model_loker.ResponseGet;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by USER on 21/03/2018.
 */

public interface SendDataLoker {

    //get for dashboard
    @Multipart
    @POST("idrekan/apiidrekan/get_data_all")
    Call<ResponseGet> getItemAll(@Part("token_m") RequestBody token_m);

}
