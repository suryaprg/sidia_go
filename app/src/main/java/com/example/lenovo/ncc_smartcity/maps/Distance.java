package com.example.lenovo.ncc_smartcity.maps;

/**
 * Created by USER on 05/02/2018.
 */

public class Distance {
    public String text;
    public int value;

    public Distance(String text, int value) {
        this.text = text;
        this.value = value;
    }
}